﻿using Planetbase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace NoIntruders
{
    public class NoIntruders : IMod
    {
        public void Init()
        {
            Debug.Log("[MOD] NoIntruders activated");
        }

        public void Update()
        {
            List<Character> intruders = Character.getSpecializationCharacters(SpecializationList.IntruderInstance);
            if (intruders != null)
            {
                foreach (Character intruder in intruders)
                {
                    // kill intruders instantly.
                    if (intruder != null && !intruder.isDead())
                    {
                        intruder.setArmed(false);
                        intruder.setDead();

                    }
                }
            }
        }
    }
}
